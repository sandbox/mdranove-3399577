(function ($, Drupal, once) {
    'use strict';
    Drupal.behaviors.twoColMenu = {
        attach: function (context, settings) {
    
          $("ul.uswds-theme--nav-linklist-two-col").each(function() {

            const elsOne = once('twoColMenu', 'li.uswds-theme--nav-first-column', this);
            const elsTwo = once('twoColMenu', 'li.uswds-theme--nav-second-column', this);

            $(elsOne).wrapAll("<div class=\'two-column-menu-wrapper desktop:grid-col-6\'>");
            $(elsTwo).wrapAll("<div class=\'two-column-menu-wrapper desktop:grid-col-6\'>");

            const menuWrapper = once('twoColMenu', '.two-column-menu-wrapper', this);
            
            $(menuWrapper).wrapAll("<div class=\'grid-row\'>");
          });
    }
  };
})(jQuery, Drupal, once);